<?php

namespace wybwsk\captcha\facade;

use think\Facade;

/**
 * Class Captcha
 * @package wybwsk\captcha\facade
 * @mixin \wybwsk\captcha\Captcha
 */
class CaptchaApi extends Facade {
    protected static function getFacadeClass() {
        return \wybwsk\captcha\CaptchaApi::class;
    }
}
